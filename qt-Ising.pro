#-------------------------------------------------
#
# Project created by QtCreator 2014-05-24T18:29:45
#
#-------------------------------------------------

QT       += core gui printsupport svg

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = qt-Ising
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
        paintwidget.cpp \
    helperfunctions.cpp \
    qcustomplot.cpp

HEADERS  += mainwindow.h \
        paintwidget.h \
    helperfunctions.h \
    qcustomplot.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc
