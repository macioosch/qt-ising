#include <cstdlib>
#include <math.h>
#include "helperfunctions.h"

int wrapN(int i, int N)
{
    if (i >= N)
        i -= N;
    else if (i < 0)
        i += N;

    return i;
}

void initSpins(std::vector<std::vector<bool> > &spins, int N, int model)
{
    if (model == 0) {
        spins.resize(1);
        spins[0].resize(N);
    } else {
        spins.resize(N);
        for (int i=0; i<N; ++i)
            spins[i].resize(N);
    }
}

void setSpins(std::vector<std::vector<bool> > &spins, int meanValue)
{
    if (meanValue == 0)
        for (int j=0; j<(int)spins.size(); ++j)
            for (int i=0; i<(int)spins[0].size(); ++i)
                spins[j][i] = rand() % 2;
    else {
        const int v = (meanValue + 1) / 2;
        for (int j=0; j<(int)spins.size(); ++j)
            for (int i=0; i<(int)spins[0].size(); ++i)
                spins[j][i] = v;
    }
}

double dEIJ(bool i, bool j)
{ /* change of energy if we would flip spin i next to spin j */
    if (i == j)
        return 2.0;

    return -2.0;
}

double dEI(bool i, double H)
{ /* change of energy if we would flip spin i in field H */
    return 2.0 * (2*i - 1) * H;
}

void flipSpins(std::vector<std::vector<bool> > &spins, int flips, double H, double T)
{
    int N = (int) spins[0].size(), picked, pickedx, pickedy;
    double deltaE;

    if (spins.size() == 1)
        for (int i=0; i<flips; ++i) {
            picked = rand() % N;
            deltaE =  dEIJ(spins[0][picked], spins[0][wrapN(picked - 1, N)])
                    + dEIJ(spins[0][picked], spins[0][wrapN(picked + 1, N)])
                    + dEI(spins[0][picked], H);
            if (rand() < RAND_MAX * exp(-deltaE / T))
                spins[0][picked] = ! spins[0][picked];
        }
    else
        for (int i=0; i<flips; ++i) {
            pickedx = rand() % N;
            pickedy = rand() % N;
            deltaE =  dEIJ(spins[pickedx][pickedy], spins[wrapN(pickedx - 1, N)][pickedy])
                    + dEIJ(spins[pickedx][pickedy], spins[wrapN(pickedx + 1, N)][pickedy])
                    + dEIJ(spins[pickedx][pickedy], spins[pickedx][wrapN(pickedy - 1, N)])
                    + dEIJ(spins[pickedx][pickedy], spins[pickedx][wrapN(pickedy + 1, N)])
                    + dEI(spins[pickedx][pickedy], H);
            if (rand() < RAND_MAX * exp(-deltaE / T))
                spins[pickedx][pickedy] = ! spins[pickedx][pickedy];
        }
}

void computeME(const std::vector<std::vector<bool> > &spins, double &M, double &E)
{
    int N = (int) spins[0].size();
    M = E = 0.0;

    if (spins.size() > 1)
        for (int j=0; j<N; ++j)
            for (int i=0; i<N; ++i) {
                M += 2 * ((int) spins[j][i]) - 1;
                E -= ((int) (spins[j][i] == spins[j][wrapN(i+1, N)]))
                    +((int) (spins[j][i] == spins[wrapN(j+1, N)][i])) - 1;
            }
    else
        for (int i=0; i<N; ++i) {
            M += 2 * ((int) spins[0][i]) - 1;
            E -= 2 * ((int) (spins[0][i] == spins[0][wrapN(i+1, N)])) - 1;
        }

    M /= N * spins.size();
    E /= N * spins.size();
}

void spinImage(const std::vector<std::vector<bool> > &spins, QImage &image)
{
    static const QRgb myRed  = qRgb( 64,  64,  64),
                      myBlue = qRgb(192, 192, 192);
    int ySpin, N = (int) spins[0].size(), spinYSize = (int) (spins.size() > 1);
    QRgb *line;

    for (int y=0; y<N; ++y) {
        line = (QRgb *)image.scanLine(y);
        ySpin = y * spinYSize;
        for (int x=0; x<N; ++x) {
            if (spins[ySpin][x])
                *line = myRed;
            else
                *line = myBlue;
            ++line;
        }
    }
}
