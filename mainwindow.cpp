#include <cstdlib>
#include <ctime>
#include <QFileDialog>
#include <QMessageBox>
#include <QImageWriter>
#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    connect(ui->pushButtonResetM0, SIGNAL(clicked()), this, SLOT(resetSimulationM0()));
    connect(ui->pushButtonResetMN, SIGNAL(clicked()), this, SLOT(resetSimulationMN()));
    connect(ui->pushButtonResetMP, SIGNAL(clicked()), this, SLOT(resetSimulationMP()));
    connect(ui->pushButtonH0, SIGNAL(clicked()), this, SLOT(zeroH()));
    connect(ui->pushButtonStartScan, SIGNAL(clicked()), this, SLOT(plotScan()));
    connect(ui->pushButtonSaveAs, SIGNAL(clicked()), this, SLOT(saveFile()));
    connect(ui->comboBoxModel, SIGNAL(currentIndexChanged(int)), this, SLOT(updateLabelsAndReset()));
    connect(ui->horizontalSliderN, SIGNAL(valueChanged(int)), this, SLOT(updateLabelsAndReset()));
    connect(ui->horizontalSliderH, SIGNAL(valueChanged(int)), this, SLOT(updateLabels()));
    connect(ui->horizontalSliderT, SIGNAL(valueChanged(int)), this, SLOT(updateLabels()));
    connect(ui->horizontalSliderFlips, SIGNAL(valueChanged(int)), this, SLOT(updateLabels()));
    connect(ui->horizontalSliderPlotPoints, SIGNAL(valueChanged(int)), this, SLOT(updateLabels()));
    connect(ui->tabWidget, SIGNAL(currentChanged(int)), this, SLOT(tabSwitch(int)));

    srand(time(0));

    ui->svgWidgetMaths->load(QString::fromUtf8(":/resources/maths.svg"));

    updateLabelsAndReset();

    plotTimer = new QTimer(this);
    connect(plotTimer, SIGNAL(timeout()), this, SLOT(updatePaintWidgets()));
    plotTimer->start(100);

    simTimer = new QTimer(this);
    connect(simTimer, SIGNAL(timeout()), this, SLOT(simulate()));
    simTimer->start(100);

    measureTimer = new QTimer(this);
    connect(measureTimer, SIGNAL(timeout()), this, SLOT(measure()));
    simTimer->start(100);

    tabSwitch(ui->tabWidget->currentIndex());

    for (int i=0; i<3; ++i)
        ui->customPlotHScan->addGraph();
    ui->customPlotHScan->graph(0)->setPen(QPen(QColor(218,  33,  60)));
    ui->customPlotHScan->graph(1)->setPen(QPen(QColor( 21, 125, 229)));
    ui->customPlotHScan->graph(2)->setPen(QPen(QColor(128, 128, 128)));
    ui->customPlotHScan->graph(0)->setName("Starting from M* = +1");
    ui->customPlotHScan->graph(1)->setName("Starting from M* = -1");
    ui->customPlotHScan->graph(2)->setName("Starting from M* = 0");
    ui->customPlotHScan->legend->setVisible(true);
    ui->customPlotHScan->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignBottom|Qt::AlignRight);
    ui->customPlotHScan->xAxis->setLabel("h*");
    ui->customPlotHScan->yAxis->setLabel("M*");
    ui->customPlotHScan->xAxis->setRange(-5, 5);
    ui->customPlotHScan->yAxis->setRange(-1.2, 1.2);
    ui->customPlotHScan->xAxis->setAutoTickStep(false);
    ui->customPlotHScan->yAxis->setAutoTickStep(false);
    ui->customPlotHScan->xAxis->setTickStep(1.0);
    ui->customPlotHScan->yAxis->setTickStep(0.2);

    ui->customPlotTScan->addGraph();
    ui->customPlotTScan->xAxis->setLabel("T*");
    ui->customPlotTScan->yAxis->setLabel("M*");
    ui->customPlotTScan->xAxis->setRange(0, 10);
    ui->customPlotTScan->yAxis->setRange(-1.2, 1.2);
    ui->customPlotTScan->xAxis->setAutoTickStep(false);
    ui->customPlotTScan->yAxis->setAutoTickStep(false);
    ui->customPlotTScan->xAxis->setTickStep(1.0);
    ui->customPlotTScan->yAxis->setTickStep(0.2);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::resetSimulationM0()
{
    initSpins(spins, p.N, p.model);
    setSpins(spins, 0);
}

void MainWindow::resetSimulationMN()
{
    initSpins(spins, p.N, p.model);
    setSpins(spins, -1);
}

void MainWindow::resetSimulationMP()
{
    initSpins(spins, p.N, p.model);
    setSpins(spins, +1);
}

void MainWindow::updateLabels()
{
    p.N = ui->horizontalSliderN->value();
    p.H = ui->horizontalSliderH->value() / 100.0;
    p.T = pow(10.0, ui->horizontalSliderT->value() / 100.0);
    p.model = ui->comboBoxModel->currentIndex();
    p.flips =  (int) ceil(pow(10.0, ui->horizontalSliderFlips->value() / 100.0)
                          * pow(p.N, 1+p.model));

    ui->labelN->setText(QString("n = %L1").arg(p.N));
    ui->labelH->setText(QString("h* = %L1").arg(p.H, 0, 'f', 2));
    ui->labelT->setText(QString("T* = %L1").arg(p.T, 0, 'f', 3));
    ui->labelFlips->setText(QString("Flips per step: %L1").arg(
                                (double) p.flips, 0, 'f', 0));
    ui->labelPlotPoints->setText(QString("Plot points: %L1").arg(
            (double) ui->horizontalSliderPlotPoints->value(), 0, 'f', 0));
}

void MainWindow::updateLabelsAndReset()
{
    updateLabels();
    resetSimulationM0();
}

void MainWindow::simulate()
{
    flipSpins(spins, p.flips, p.H, p.T);
}

void MainWindow::updatePaintWidgets()
{
    ui->paintWidget->spins = spins;
    ui->paintWidget->model = p.model;
    ui->paintWidget->update();

    measure();
}

void MainWindow::measure()
{
    double M, E;
    computeME(spins, M, E);

    ui->labelEnergy->setText(QString("<E*> = %L1").arg(E, 0, 'f', 3));
    ui->labelMagnetization->setText(QString("<M*> = %L1").arg(M, 0, 'f', 3));
    ui->progressBarE->setValue(-100.0*E);
    ui->progressBarM->setValue( 100.0*M);
}

void MainWindow::tabSwitch(int tab)
{
    ui->groupBoxVis->setVisible(tab == 0);
    ui->groupBoxScan->setVisible(tab != 0);
    ui->horizontalSliderH->setVisible(tab != 1);
    ui->labelH->setVisible(tab != 1);
    ui->horizontalSliderT->setVisible(tab != 2);
    ui->labelT->setVisible(tab != 2);

    if (tab == 0)
        ui->pushButtonSaveAs->setText("Save image as...");
    else
        ui->pushButtonSaveAs->setText("Save graph as...");

    if (tab == 0) {
        plotTimer->start();
        simTimer->start();
        measureTimer->start();
    } else {
        plotTimer->stop();
        simTimer->stop();
        measureTimer->stop();
    }
}

void MainWindow::zeroH()
{
    ui->horizontalSliderH->setValue(0);
}

void MainWindow::plotScan()
{
    ui->pushButtonStartScan->setEnabled(false);

    const int plotPoints = ui->horizontalSliderPlotPoints->value();
    double H, T, M, E;
    QVector<double> x(plotPoints), y1(plotPoints), y2(plotPoints), y3(plotPoints);
    std::vector<std::vector<bool> > s, sP, sN;
    initSpins(s, p.N, p.model);
    initSpins(sP, p.N, p.model);
    initSpins(sN, p.N, p.model);
    setSpins(sP, +1);
    setSpins(sN, -1);

    ui->progressBarScan->setValue(0);

    if (ui->tabWidget->currentIndex() == 1) { /* M*(h*) plot */
        for (int i=0; i<plotPoints; ++i) {
            x[i] = H = -5.0 + 10.0 * i / (plotPoints-1.0);

            s = sP;
            flipSpins(s, p.flips, H, p.T);
            computeME(s, M, E);
            y1[i] = M;

            s = sN;
            flipSpins(s, p.flips, H, p.T);
            computeME(s, M, E);
            y2[i] = M;

            setSpins(s, 0);
            flipSpins(s, p.flips, H, p.T);
            computeME(s, M, E);
            y3[i] = M;

            ui->progressBarScan->setValue(100.0 * (i+1.0) / plotPoints);
        }
        ui->customPlotHScan->graph(0)->setData(x, y1);
        ui->customPlotHScan->graph(1)->setData(x, y2);
        ui->customPlotHScan->graph(2)->setData(x, y3);
        ui->customPlotHScan->replot();
    } else if (ui->tabWidget->currentIndex() == 2) { /* M*(T*) plot */
        for (int i=0; i<plotPoints; ++i) {
            x[i] = T = 10.0 * (i + 1.0) / plotPoints;

            s = sP;
            flipSpins(s, p.flips, p.H, T);
            computeME(s, M, E);
            y1[i] = M;

            ui->progressBarScan->setValue(100.0 * (i+1.0) / plotPoints);
        }

        ui->customPlotTScan->graph(0)->setData(x, y1);
        ui->customPlotTScan->replot();
    }

    ui->pushButtonStartScan->setEnabled(true);
}

void MainWindow::saveFile()
{
    plotTimer->stop();
    simTimer->stop();
    measureTimer->stop();

    QString formats = "*.png";
    if (ui->tabWidget->currentIndex() != 0)
        formats.append(";;*.pdf");
    QMessageBox warningNotSavedBox(this);
    warningNotSavedBox.setWindowTitle("Warning!");
    bool success = false;

    QString fileName = QFileDialog::getSaveFileName(this, "Save as...", QString(), formats);

    if (ui->tabWidget->currentIndex() == 0) {
        QImageWriter imageWriter;
        imageWriter.setFileName(fileName);
        imageWriter.setFormat("png");
        if (imageWriter.canWrite()) {
            QImage image = QImage(p.N, p.N, QImage::Format_RGB32);
            spinImage(spins, image);
            success = imageWriter.write(image);
        }
    } else if (ui->tabWidget->currentIndex() == 1) {
        if (fileName.endsWith(".png", Qt::CaseInsensitive))
            success = ui->customPlotHScan->savePng(fileName);
        else if (fileName.endsWith(".pdf", Qt::CaseInsensitive))
            success = ui->customPlotHScan->savePdf(fileName, true);
    } else if (ui->tabWidget->currentIndex() == 2) {
        if (fileName.endsWith(".png", Qt::CaseInsensitive))
            success = ui->customPlotTScan->savePng(fileName);
        else if (fileName.endsWith(".pdf", Qt::CaseInsensitive))
            success = ui->customPlotTScan->savePdf(fileName, true);
    }

    if (! success) {
        warningNotSavedBox.setText("The file \""+fileName+"\" couldn't be saved.");
        warningNotSavedBox.exec();
    }

    plotTimer->start();
    simTimer->start();
    measureTimer->start();
}
