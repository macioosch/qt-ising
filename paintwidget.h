#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QTimer>

class PaintWidget : public QWidget
{
    Q_OBJECT
public:
    explicit PaintWidget(QWidget *parent = 0);
    std::vector<std::vector<bool> > spins;
    int model;

protected:
    void paintEvent(QPaintEvent *event);

private:
    void paintVisualisation(QPainter &painter);
};

#endif // PAINTWIDGET_H
