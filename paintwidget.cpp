#include <math.h>
#include "paintwidget.h"
#include "helperfunctions.h"

PaintWidget::PaintWidget(QWidget *parent) :
    QWidget(parent)
{
    spins.resize(1);
    spins[0].resize(1);
}

void PaintWidget::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    // brush and colors:
    static QPalette palette;
    painter.setBrush(palette.window());
    painter.setRenderHint(QPainter::Antialiasing, false);
    painter.setPen(QPen(Qt::black, 0.0));

    paintVisualisation(painter);
}

void PaintWidget::paintVisualisation(QPainter &painter)
{
    int N = (int) spins[0].size();
    static QImage image;

    if (image.width() != N)
        image = QImage(N, N, QImage::Format_RGB32);

    spinImage(spins, image);

    if (width() >= height())
        painter.drawImage(QRect((width()-height())/2, 0, height(), height()), image);
    else
        painter.drawImage(QRect(0, (height()-width())/2, width(), width()), image);
}
