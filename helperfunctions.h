#ifndef HELPERFUNCTIONS_H
#define HELPERFUNCTIONS_H

#include <vector>
#include <QImage>

typedef struct {
    int flips, model, N;
    double H, T;
} params;

int wrapN(int i, int N);

void initSpins(std::vector<std::vector<bool> > &spins, int N, int model);
void setSpins(std::vector<std::vector<bool> > &spins, int meanValue);
void flipSpins(std::vector<std::vector<bool> > &spins, int flips, double H, double T);
void computeME(const std::vector<std::vector<bool> > &spins, double &M, double &E);
void spinImage(const std::vector<std::vector<bool> > &spins, QImage &image);

#endif // HELPERFUNCTIONS_H
