#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "helperfunctions.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    std::vector<std::vector<bool> > spins;
    QTimer *plotTimer, *simTimer, *measureTimer;
    params p;

private slots:
    void resetSimulationM0();
    void resetSimulationMN();
    void resetSimulationMP();
    void updateLabels();
    void updateLabelsAndReset();
    void simulate();
    void updatePaintWidgets();
    void measure();
    void tabSwitch(int tab);
    void zeroH();
    void plotScan();
    void saveFile();
};

#endif // MAINWINDOW_H
